package machine

import "sync"

type runnerDetails struct {
	growing        int
	growthCondLock sync.Mutex
	growthCond     *sync.Cond

	available chan struct{}
}

func newRunnerDetails() *runnerDetails {
	result := runnerDetails{
		available: make(chan struct{}),
	}
	result.growthCond = sync.NewCond(&result.growthCondLock)

	return &result
}

func (r *runnerDetails) WaitForGrowthCapacity(maxGrowth int, f func()) {
	r.growthCondLock.Lock()
	for maxGrowth != 0 && r.growing >= maxGrowth {
		r.growthCond.Wait()
	}

	r.growing++
	r.growthCondLock.Unlock()

	defer func() {
		r.growthCondLock.Lock()
		r.growing--
		r.growthCondLock.Unlock()
		r.growthCond.Signal()
	}()

	f()
}

func (r *runnerDetails) WaitForAvailableMachine() {
	<-r.available
}

func (r *runnerDetails) AvailablitySignal() chan struct{} {
	return r.available
}

func (r *runnerDetails) ReleaseMachine() {
	select {
	case r.available <- struct{}{}:
	default:
	}
}

type runnersDetails map[string]*runnerDetails
